﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GeometryDash.Core.MyGame;
using System.IO;
using Color = Microsoft.Xna.Framework.Color;
using Rectangle = Microsoft.Xna.Framework.Rectangle;
using Microsoft.Xna.Framework.Input.Touch;

namespace GeometryDash.Core
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Level currentLevel;
        private KeyboardState keyboardState;
        private TouchCollection touchCollection;

        private Vector2 baseScreenSize = new Vector2(1100, 550);
        private Matrix globalTransformation;

        public static int backbufferWidth; // largeur
        public static int backbufferHeight;// hauteur

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);

            _graphics.ToggleFullScreen();

            IsMouseVisible = true;
        }

        protected override void LoadContent()
        {
            Content.RootDirectory = "Content";

            _spriteBatch = new SpriteBatch(GraphicsDevice);

            ScalePresentationArea();

            LoadLevel("0");
        }

        public void ScalePresentationArea()
        {
            //Work out how much we need to scale our graphics to fill the screen
            backbufferWidth = GraphicsDevice.PresentationParameters.BackBufferWidth;
            backbufferHeight = GraphicsDevice.PresentationParameters.BackBufferHeight;
            float horScaling = backbufferWidth / baseScreenSize.X;
            float verScaling = backbufferHeight / baseScreenSize.Y;
            Vector3 screenScalingFactor = new Vector3(horScaling, verScaling, 1);
            globalTransformation = Matrix.CreateScale(screenScalingFactor);
            System.Diagnostics.Debug.WriteLine("Screen Size - Width[" + GraphicsDevice.PresentationParameters.BackBufferWidth + "] Height [" + GraphicsDevice.PresentationParameters.BackBufferHeight + "]");
        }

        private void LoadLevel(string level)
        {
            string levelPath = string.Format("Content/Levels/{0}.txt", level);
            using (Stream fileStream = TitleContainer.OpenStream(levelPath))
                currentLevel = new Level(_spriteBatch, fileStream, backbufferWidth, backbufferHeight, globalTransformation, this);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape) || currentLevel.ReachedExit) { Exit(); }

            HandleInput();

            currentLevel.Update(gameTime, keyboardState, touchCollection);

            base.Update(gameTime);
        }

        private void HandleInput()
        {
            keyboardState = Keyboard.GetState();
            touchCollection = TouchPanel.GetState();
        }

        protected override void Draw(GameTime gameTime)
        {
            _graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(currentLevel.background, new Rectangle(0, 0, backbufferWidth, (currentLevel.background.Height*backbufferWidth)/currentLevel.background.Width), Color.BlueViolet);
            _spriteBatch.End();

            _spriteBatch.Begin(transformMatrix: currentLevel.camera.Transform);
            currentLevel.Draw(gameTime, _spriteBatch);
            _spriteBatch.End();

            base.Draw(gameTime);
        }

        
    }
}
