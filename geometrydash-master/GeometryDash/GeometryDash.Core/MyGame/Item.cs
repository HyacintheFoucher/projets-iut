﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeometryDash.Core.MyGame
{
    class Item : GameObject
    {
        private Texture2D _texture;
        private string item; //type d'item (block/spike)
        public Vector2 Position { get; private set; }

        public Rectangle BoundingRectangle
        {
            get {return boundingRectangle; }
        }
        private Rectangle boundingRectangle =>
            new Rectangle((int)Position.X, (int)Position.Y, _texture.Width, _texture.Height);

        public Item(string item, Vector2 position, SpriteBatch spriteBatch, Game1 game) : base(game, spriteBatch)
        {

            this.item = item;
            Position = new Vector2(position.X * 55, position.Y * 55);
            LoadContent();            
        }

        protected override void LoadContent()
        {
            _texture = Game.Content.Load<Texture2D>($"objects/{item}");
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Draw(_texture, Position, Color.White);
        }

        public Rectangle GetItemBounds()
        {
            return BoundingRectangle;
        }

        public bool isDeadlyItem()
        {
            if(item.StartsWith("spike") )
            {
                return true;
            }
            return false;
        }

    }
}
