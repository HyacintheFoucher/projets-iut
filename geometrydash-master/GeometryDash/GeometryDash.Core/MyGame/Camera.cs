﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeometryDash.Core.MyGame
{
    public class Camera
    {
        private Matrix globalTransformation;

        public Matrix Transform { get; private set; }

        public Camera(Matrix globalTransformation)
        {
            this.globalTransformation = globalTransformation;
        }

        public void Follow(Rectangle r)
        {
            var position = Matrix.CreateTranslation(
                - r.X - (r.Width / 2),
                - r.Y - (r.Height / 2)
                 , 0) * globalTransformation;

            var offset = Matrix.CreateTranslation(Game1.backbufferWidth / 4, (int)Game1.backbufferHeight / 1.5f, 0);

            Transform = position * offset;
        }
    }
}
