﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GeometryDash.Core.MyGame
{
    public abstract class GameObject : DrawableGameComponent
    {
        protected readonly SpriteBatch _spriteBatch;

        public GameObject(Game1 game, SpriteBatch spritebatch) : base(game)
        {
            _spriteBatch = spritebatch;
        }
    }
}