﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.IO;

namespace GeometryDash.Core.MyGame
{
    class Level
    {
        private int widthLevel;
        private int heightLevel;

        private Item[,] _items;
        public Vector2 start { get; private set; }

        public Texture2D background { get; private set; }

        public Player player { get; private set; }

        public Camera camera { get; private set; }

        public bool ReachedExit
        {
            get { return reachedExit; }
        }
        bool reachedExit;

        public Level( SpriteBatch spriteBatch, Stream fileStream, int w, int h, Matrix globalTransformation, Game1 game)
        {
            
            LoadGameObjects(spriteBatch, fileStream, globalTransformation, game);

            background = game.Content.Load<Texture2D>($"Backgrounds/game_bg_01_001");

            camera = new Camera(globalTransformation);
        }

        private void LoadGameObjects(SpriteBatch spriteBatch, Stream fileStream, Matrix globalTransformation, Game1 game)
        {
            
            List<string> lines = new List<string>();

            using (StreamReader reader = new StreamReader(fileStream))
            {
                string[] firstLine = reader.ReadLine().Split(' ');

                widthLevel = int.Parse(firstLine[0]);
                heightLevel = int.Parse(firstLine[1]);
                _items = new Item[widthLevel,heightLevel];

                string line = reader.ReadLine();
                while (line != null)
                {
                    lines.Add(line);
                    if (line.Length != widthLevel)
                        throw new Exception(String.Format("The length of line {0} is different from all preceeding lines.", lines.Count));
                    line = reader.ReadLine();
                }   
            }

            for (int y = 0; y < heightLevel; ++y)
            {
                for (int x = 0; x < widthLevel; ++x)
                {
                    char ItemType = lines[y][x];
                    _items[x,y] = LoadItem(ItemType, x, y, spriteBatch, game) ;
                }
            }

            if (player.Equals(null))
            {
                throw new Exception(String.Format("The player isn't in the level"));
            }
        }

        private Item LoadItem(char itemType, int x, int y,SpriteBatch spriteBatch, Game1 game)
        {
            switch (itemType)
            {
                case '.':
                    return null;
                case 'B':
                    return LoadBlock(x, y, spriteBatch, game);
                case 'S':
                   return LoadSpike(x, y, spriteBatch, game);
                case '1':
                    LoadPlayer(x, y, spriteBatch, game);
                    return null;
                default:
                    return null;
            }
        }

        private void LoadPlayer(int x, int y, SpriteBatch spriteBatch, Game1 game)
        {
            start = new Vector2(x, y);
            player = new Player(start, widthLevel, heightLevel, "4", spriteBatch, this, game); //TODO
        }

        private Item LoadSpike(int x, int y, SpriteBatch spriteBatch, Game1 game)
        {
            return new Item("spike1", new Vector2(x, y), spriteBatch, game);
        }

        private Item LoadBlock(int x, int y, SpriteBatch spriteBatch, Game1 game)
        {
            return new Item("block1", new Vector2(x, y), spriteBatch, game);
        }


        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            for (int y = 0; y < heightLevel; ++y)
            {
                for (int x = 0; x < widthLevel; ++x)
                {
                    if(_items[x,y] != null)
                    {
                        _items[x,y].Draw(gameTime);
                    }
                }
            }

            player.Draw(gameTime);
        }

        public void Update(GameTime gameTime, KeyboardState keyboardState, TouchCollection touchCollection)
        {
            if (!player.IsAlive)
            {
                player.Reset();
            }
            else if (player.ReachedExit)
            {

                reachedExit = true;
            }

            player.Update(gameTime, keyboardState, touchCollection);

            camera.Follow(player.BoundingRectangle);
        }
 
        public Rectangle GetCollision(int x, int y)
        {
            if(_items[x, y] != null)
            {
                return _items[x, y].BoundingRectangle;
            }
            return Rectangle.Empty;
        }

        public bool isDeadly(int x, int y)
        {
            return _items[x, y].isDeadlyItem();
        }
    }
}
