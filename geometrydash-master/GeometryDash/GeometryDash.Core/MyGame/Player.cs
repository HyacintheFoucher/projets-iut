﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;

namespace GeometryDash.Core.MyGame
{
    class Player : GameObject
    {
        //texture
        private Texture2D _texture;
        private string _skin;

       private Level _level;

        public bool IsAlive {
            get { return isAlive; }
        }
        bool isAlive = true;

        public Vector2 Position { get; private set; }

        private Vector2 _start;

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        Vector2 velocity;

        
        private float Speed = 5f;
        private float relativeStartX;
        private const float Gravity = 9.81f;

        public bool IsOnGround
        {
            get { return isOnGround; }
        }
        bool isOnGround;

        private bool isJumping;
        private bool wasJumping;
        private float jumpSize;
        private float jumpLength = 0.0f;
        private float jumpTime = 0.0f;
        private double a = -1 / 110f;

        private int maxX;
        private int maxY;

        public bool ReachedExit { get; private set; }
        public Rectangle BoundingRectangle
        {
            get { return boundingRectangle; }
        }
        private Rectangle boundingRectangle => 
            new Rectangle((int)Position.X, (int)Position.Y, _texture.Width, _texture.Height);

        

        public Player(Vector2 position, int weight, int height, string skin, SpriteBatch spriteBatch, Level level, Game1 game) :base(game, spriteBatch)
        {
            _level = level;

            maxX = (int)weight;
            maxY = (int)height;

            Position = new Vector2(position.X * 55, position.Y * 55);
            _start = Position;

            //pour le saut (calcul des differents facteurs utiles a la création d'une belle parabole)
            relativeStartX = 110 * (Speed / 4.5f);
            a = -110f / (relativeStartX * relativeStartX);
            
            _skin = skin;

            LoadContent();
        }

        protected override void LoadContent()
        {
            _texture = Game.Content.Load<Texture2D>($"Sprites/block{_skin}");
        }

        

        public void Update(GameTime gameTime, KeyboardState keyboardState, TouchCollection touchCollection)
        {
            GetInput(keyboardState, touchCollection);
            ApplyPhysics(gameTime);
        }

        private void GetInput(KeyboardState keyboardState, TouchCollection touchCollection)
        {
#if ANDROID
            isJumping = touchCollection.Count > 0;
#else
            isJumping = keyboardState.IsKeyDown(Keys.Space);
#endif

        }

        public void ApplyPhysics(GameTime gameTime)
        {
            float deltaTime = ((float)gameTime.ElapsedGameTime.TotalMilliseconds)/10f; 

            velocity.X = Speed * deltaTime;
            velocity.Y = Gravity * deltaTime;

            velocity.Y = DoJump(velocity.Y, deltaTime);

            Position += velocity;
            Position = new Vector2((float)Math.Round(Position.X), (float)Math.Round(Position.Y));

            HandleCollisions();

        }

        private float DoJump(float velocityY, float deltaTime)
        {
            if (isJumping || jumpTime > 0.0f)
            {
                if ((!wasJumping && IsOnGround) || jumpTime > 0.0f)
                {
                    
                    jumpTime += Velocity.X;
                    jumpLength = (float)((a) * ((jumpTime - relativeStartX) * (jumpTime - relativeStartX)) + 110) - jumpSize;
                    jumpSize += jumpLength;
                }
                if (0.0f < jumpTime && (!isOnGround ||(isJumping && !wasJumping && IsOnGround) ) )
                {
                    velocityY = -jumpLength;
                }
                else
                {
                    // Reached the ground after the jump
                    jumpTime = 0.0f;
                    jumpLength = 0.0f;
                    jumpSize = 0.0f;
                    isJumping = false;
                }
            }
            wasJumping = isJumping;
            
            return velocityY;
        } 

        private void HandleCollisions()
        {
            Rectangle bounds = BoundingRectangle;
            int leftItem = (int)(bounds.Left / 55f) - 1;
            int rightItem = (int)(bounds.Right / 55f);
            int topItem = (int)(bounds.Top / 55f) -1;
            int bottomItem = (int)(bounds.Bottom / 55f);

            leftItem = leftItem > 0 ? leftItem : 0;
            topItem = topItem > 0 ? topItem : 0;
            bottomItem = bottomItem < maxY - 1 ? bottomItem : (maxY - 1);

            if(rightItem >= maxX - 1)
            {
                ReachedExit = true;
                return;
            }

            isOnGround = false;

            for (int y = topItem; y <= bottomItem; ++y)
            {
                for (int x = leftItem; x <= rightItem; ++x)
                {
                    
                    Rectangle itemBounds = _level.GetCollision(x, y);
                    if (!itemBounds.IsEmpty)
                    {

                        Rectangle r = Rectangle.Intersect(bounds, itemBounds);
                        if (!r.IsEmpty)
                        {
                            if(y == bottomItem)
                            {
                                if (_level.isDeadly(x, y))
                                {
                                    isAlive = false;
                                }
                                else
                                {
                                    float localPosY = Position.Y / 55;
                                    if (localPosY != (int)localPosY)
                                    {
                                        Position = new Vector2(Position.X, (int)Math.Round(Position.Y - r.Height));
                                    }
                                    isOnGround = true;
                                }
                            }
                            else
                            {
                                isAlive = false;
                            }

                        }
                    }
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Draw(_texture, Position, Color.White);
        }

        public void Reset()
        {
            Position = _start;
            isAlive = true;
        }
    }

}
